#include <Wire.h>
#include <math.h>

#define BSD_ADDRESS 0x77  // I2C address of 89BSD

// Calibration values
int data[7] = {0, 0, 0, 0, 0, 0, 0};
int c0=0;
int c1=0; 
int c2=0; 
int c3=0;
int c4=0;
int c5=0;
int c6=0; 
int a0=0;
int a1=0;
int a2=0;

unsigned long d1;
unsigned long d2;

double temp;
double y;
double p;
double pressure;

void setup() {
  Wire.begin();
  Serial.begin(9600);
  Wire.beginTransmission(BSD_ADDRESS);
  Wire.write(0x1E);                        //reset command
  Wire.endTransmission();
  delay(100);                      
  
  Calibration();                           //get calibration values
  
  Serial.println("calibration done");
}

void loop() {
  d1 = ReadADC(0x44);
  Serial.print("digital pressure value: ");
  Serial.println(d1);
  d2 = ReadADC(0x54);
  Serial.print("digital temperature value: ");
  Serial.println(d2);
  Compensate();
  Serial.print("actual temperature: ");
  Serial.println(temp);  
  Serial.print("compensated pressure: ");
  Serial.println(pressure);
  delay(1000);

}


void Calibration()
{
  ReadCalib();
  ReadBits(c0, 1, 15, 14);
  ReadBits(c1, 1, 1, 14);
  ReadBits(c2, 2, 3, 10);
  ReadBits(c3, 3, 9, 10);
  ReadBits(c4, 4, 15, 10);
  ReadBits(c5, 4, 5, 10);
  ReadBits(c6, 5, 11, 10);
  ReadBits(a0, 5, 1, 10);
  ReadBits(a1, 6, 7, 10);
  ReadBits(a2, 7, 13, 10);
  
  Serial.println("Calibration Parameters:");
  Serial.print("c0 = ");
//  Serial.println(c0, BIN);
  Serial.println(c0);
  Serial.print("c1 = ");
  Serial.println(c1);
  Serial.print("c2 = ");
  Serial.println(c2);
  Serial.print("c3 = ");
  Serial.println(c3);
  Serial.print("c4 = ");
  Serial.println(c4);
  Serial.print("c5 = ");
  Serial.println(c5);
  Serial.print("c6 = ");
  Serial.println(c6);
  Serial.print("a0 = ");
  Serial.println(a0);
  Serial.print("a1 = ");
  Serial.println(a1);
  Serial.print("a2 = ");
  Serial.println(a2);
}

void ReadBits(int &result, int address_num, int bitbegin, int len)
{
  address_num-=1;

  if (bitRead(data[address_num], bitbegin) == 1) {
    result = -1;      //invert all bits since value is negative
//    Serial.print("negativer wert. setze result auf: ");
//    Serial.println(result, BIN);
  }
  
  int counter = 0;
  int counter2 = 0;

  for (int i = bitbegin; (i >= 0) && counter < len; i--)
  {
    bitWrite(result, len - 1 - counter, bitRead(data[address_num], i));
    counter++;
  }
  while (counter < len)
  {
    bitWrite(result, len - 1 - counter, bitRead(data[address_num + 1], 15 - counter2));
    counter++;
    counter2++;
  }
}

void ReadCalib()
{
  byte firstbyte, secondbyte;
  for (int i = 0; i < 7; i++)
  {
    Wire.beginTransmission(BSD_ADDRESS);
    Wire.write(0xA2 + 2*i);
    Wire.endTransmission();
    delay(20);
  
    Wire.requestFrom(BSD_ADDRESS, 2);
    firstbyte = Wire.read();
    secondbyte = Wire.read();
//    Serial.println("reading calib");
//    Serial.println(firstbyte, BIN);
//    Serial.println(secondbyte, BIN); 
//    Serial.println(data[i], BIN);   
    data[i] = firstbyte<<8 | secondbyte;
//    data[i] = secondbyte<<8 | firstbyte;
//    Serial.println(sizeof(data[i]));
//    Serial.println(data[i], BIN);
//    Serial.println("read calib done");
  }
}

unsigned long ReadADC(unsigned char address)
{
  byte firstbyte, secondbyte, thirdbyte;
//  Serial.println(address, HEX);
  
  Wire.beginTransmission(BSD_ADDRESS);
  Wire.write(address);
  Wire.endTransmission();
  delay(20);
  Wire.beginTransmission(BSD_ADDRESS);
  Wire.write(0x00);
  Wire.endTransmission();
  delay(20);
  
  Wire.requestFrom(BSD_ADDRESS, 3);
  delay(20);
  Serial.print("ADC Read at adress 0x");
  Serial.println(address, HEX);
  
  firstbyte = Wire.read();
  secondbyte = Wire.read();
  thirdbyte = Wire.read();
  delay(10);
  
//  Serial.println(eins, BIN);
//  Serial.println(zwei, BIN);
//  Serial.println(drei, BIN);
//  unsigned long check = eins<<16 | zwei<<8 | drei;
//  Serial.println(check, BIN);
  
  return (unsigned long) firstbyte<<16 | secondbyte<<8 | thirdbyte;
}

//unsigned long ReadPressure(unsigned char address)
//{
//  byte eins, zwei, drei;
//  
//  Wire.beginTransmission(BSD_ADDRESS);
//  Wire.write(address);
//  delay(20);
//  Wire.write(0x00);
//  Wire.endTransmission();
//  delay(20);
//  
//  Wire.requestFrom(BSD_ADDRESS, 3);
//  delay(20);
//  Serial.println("read pressure");
//  
//  eins = Wire.read();
//  zwei = Wire.read();
//  drei = Wire.read();
//  delay(10);
//  
//  return (unsigned long) eins<<16 | zwei<<8 | drei;
//}

void Compensate()
{
  temp = a0 / 5 + a1 * 2 * d2 / pow(2, 24) + a2 * 2 * pow(d2 / pow(2, 24), 2);
  
  double num = (d1 + c0 * pow(2, 9) + c3 * pow(2, 15) * d2 / pow(2, 24) + c4 * pow(2, 15) * pow(d2 / pow(2, 24), 2));
  double denum = (c1 * pow(2, 11) + c5 * pow(2, 16) * d2 / pow(2, 24) + c6 * pow(2, 16) * pow(d2 / pow(2, 24), 2));
  y = num/denum;
  p = y * (1 - c2 * pow(2, 9) / pow(2, 24)) + c2 * pow(2, 9) / pow(2, 24) * pow(y, 2);
  Serial.print("y = ");
  Serial.println(y);
  Serial.print("p = ");
  Serial.println(p);
  pressure = (p - 0.1) / 0.8 * (6 - 0) + 0;
}
