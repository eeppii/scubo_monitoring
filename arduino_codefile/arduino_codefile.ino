//johann, 1. june 2016
//timo, 13. june 2016

#if defined(ARDUINO) && ARDUINO >= 100
  #include "Arduino.h"
#else
  #include <WProgram.h>
#endif

#define USE_USBCON      //needed when running on the UDOO or on Arduino Due, comment out for Arduino Uno
#define BSD_ADDRESS 0x77      //I2C address of 89BSD pressure sensor

#include <Servo.h> 
#include <ros.h>
#include <ArduinoHardware.h>
#include <std_msgs/String.h>
#include <std_msgs/UInt16MultiArray.h>
#include <std_msgs/UInt16.h>
#include <std_msgs/Float32.h>
#include <std_msgs/Float64.h>
#include <Wire.h>
#include <Adafruit_PWMServoDriver.h>
#include <math.h>

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

unsigned int hz_pressure = 100; //here you can set the rate of pressure sensor reading in Hz
unsigned int millis_pressure = 1000 / hz_pressure;

int data[7] = {0, 0, 0, 0, 0, 0, 0};  //pressure sensor calibration values
int c0=0;
int c1=0; 
int c2=0; 
int c3=0;
int c4=0;
int c5=0;
int c6=0; 
int a0=0;
int a1=0;
int a2=0;

unsigned long d1; //digital pressure value (uncompensated)
unsigned long d2; //digital temperature

//double temp;  //actual temperature in celsius. we don't need this, thus commented out
double p_compensated; //temperature-compensated pressure
unsigned long next_pub_time;  // used to get fixed publisher frequency

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

ros::NodeHandle_<ArduinoHardware, 2, 2, 150, 150> nh; // from: http://answers.ros.org/question/28890/using-rosserial-for-a-atmega168arduino-based-motorcontroller/, first two numbers # Publisher/Subscriber , second two numbers are buffersize

Adafruit_PWMServoDriver pwm = Adafruit_PWMServoDriver(); //default adress 0x40 called, adress of break-out board

std_msgs::Float64 float_msg;
std_msgs::Float32 pressure;
//std_msgs::Float32 bsd_temp;

ros::Publisher chatter("sensor_signal", &float_msg);  //definition sensor topic, float64
ros::Publisher publish_p("pressure_signal", &pressure);      //definition pressure topic, float32
//ros::Publisher publish_t("bsd_temp", &bsd_temp);      //temperature of the pressure sensor

unsigned long currentMillis;  //will store time when main loop begins
unsigned long previousMillis1; //will store last publish time, voltage
unsigned long previousMillis2; //will store last publish time, temperature
unsigned long previousMillis3a; //will store last publish time, leak 1
unsigned long previousMillis3b; //will store last publish time, leak 2
unsigned long previousMillis3c; //will store last publish time, leak 3
//unsigned long previousMillis4; //will store last publish time, current. cuttently not connected

float bvm_value; // initialization of a voltage variable

float resistor1=402;

float resistor2=95.2;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void pwm_cb( const std_msgs::UInt16MultiArray& cmd_msg) { //callback function of thruster allocation
  
  for(int i=0; i<8; i++)  //runs every thrusters via for-loop
  { 
     pwm.setPWM(i,0, cmd_msg.data[i]); //PWM signal, state where it goes high, state where it goes low, 0-4095, deadband 351-362
  }
}

ros::Subscriber<std_msgs::UInt16MultiArray> sub("pwm_signal", pwm_cb); //definition PWM topic and callback function

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void dim_cb( const std_msgs::UInt16& dim_msg)   // callback function for dimming
{ 

  analogWrite(9, dim_msg.data);
  analogWrite(11, dim_msg.data);
  
}

ros::Subscriber<std_msgs::UInt16> sub_dim("dimming_signal", dim_cb);

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void setup()  //this function runs first and only one time
{ 
  
  // initialize pins
  pinMode(8, OUTPUT); // for dimming
  pinMode(9, OUTPUT); // for dimming
  pinMode(10, OUTPUT); // for dimming
  pinMode(11, OUTPUT); // for dimming
  digitalWrite(8, HIGH); // for dimming initialization
  digitalWrite(10, HIGH); // for dimming initialization
  analogWrite(9, 0); // for dimming
  analogWrite(11, 0); // for dimming
  pinMode(A5, INPUT); // for current measurement

  // initialize ros nodes
  nh.initNode();
  delay(5000); //initialization after approximately 5 seconds
  nh.advertise(chatter); //for voltage sensor
  nh.advertise(publish_p); //for pressure sensor
  nh.subscribe(sub); //for thruster allocation
  nh.subscribe(sub_dim); // for dimming

  // initialize thrusters
  pwm.begin();
  pwm.setPWMFreq(60); //60 Hz PWM signal
  for(int j=0;j<8;j++)  //initialize every thruster via channel (0-7) with a for-loop 
  {  
    pwm.setPWM(j,0,351);    
  }

  // initialize pressure sensor
  Wire.begin();
  Wire.beginTransmission(BSD_ADDRESS);
  Wire.write(0x1E);                        //reset pressure sensor
  Wire.endTransmission();
  delay(10);                      
  Calibration();                           //get calibration values

  // set timers for publishing
  currentMillis=millis();
  next_pub_time = currentMillis + millis_pressure;
  previousMillis1 = currentMillis - 20000;
  previousMillis2 = currentMillis - 10000;
  previousMillis3a = currentMillis;
  previousMillis3b = currentMillis + 1000;
  previousMillis3c = currentMillis + 2000;
  
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void loop() 
{    
  currentMillis=millis();
  
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////  

  //pressure sensor reading
  
  if (next_pub_time < currentMillis)
  {
    if (next_pub_time + 10 < currentMillis) 
    {
      next_pub_time = currentMillis + millis_pressure;
    }
    else
    {
      next_pub_time += millis_pressure;
    }    
    
    d1 = ReadADC(0x44);
    d2 = ReadADC(0x54);;
    Compensate();
    //bsd_temp.data = temp;
    pressure.data = p_compensated;
    //publish_t.publish(&bsd_temp);
    publish_p.publish(&pressure);
  }

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  //voltage sensor reading
  
  if (currentMillis - previousMillis1 > 30000)
  {
    analogRead(A1);
    delay(10);  //for discharging, results in a more precise measurement
    int voltvalue = analogRead(A1);
    bvm_value = 3.3*voltvalue/1024*(resistor1/resistor2 + 1); // change this to 3.3 if running on UDOO or Arduino Due  
    float_msg.data=bvm_value + 100;  //offset 100 to distinguish from other signals
    chatter.publish(&float_msg);
    previousMillis1=currentMillis; // update time
  }

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  // temperature sensor reading
  
  if (currentMillis - previousMillis2 > 30000)
  {
    analogRead(A0);
    delay(10);  //for discharging, results in a more precise measurement
    int vo = analogRead(A0);
    float temp_value = (3.3*vo/1024 - 0.25)*1000/28 - 190*1.5/1000;  // transfer function and heat sink correction (190*1.5/1000)
    float_msg.data=temp_value + 1000; //offset 1000 to distinguish from other signals
    chatter.publish(&float_msg);
    previousMillis2=currentMillis; // update time
  }
      
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////  

  //leak sensor readings
      
  if (currentMillis - previousMillis3a > 30000)
  {           
    analogRead(A2); //leak pin
    delay(10); // delay for discharging reasons, previously 50
    int leakvalue=analogRead(A2); // defining the pins on the Arduino
    float leakage=leakvalue*(3.3/1023); // change this to 3.3 if running on UDOO or Arduino Due
    float_msg.data=leakage;
    chatter.publish(&float_msg);
    previousMillis3a=currentMillis; // update time
  }

  if (currentMillis - previousMillis3b > 30000)
  { 
    analogRead(A3); //leak pin
    delay(10); // delay for discharging reasons, previously 50
    int leakvalue=analogRead(A3); // defining the pins on the Arduino
    float leakage=leakvalue*(3.3/1023); // change this to 3.3 if running on UDOO or Arduino Due
    float_msg.data=leakage;
    chatter.publish(&float_msg);
    previousMillis3b=currentMillis; // update time
  }

  if (currentMillis - previousMillis3c > 30000)
  { 
    analogRead(A4); //leak pin
    delay(10); // delay for discharging reasons, previously 50
    int leakvalue=analogRead(A4); // defining the pins on the Arduino
    float leakage=leakvalue*(3.3/1023); // change this to 3.3 if running on UDOO or Arduino Due
    float_msg.data=leakage;
    chatter.publish(&float_msg);
    previousMillis3c=currentMillis; // update time       
  }

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////  

//  if (currentMillis - previousMillis4 > 45000)  // current sensor not connected at the moment thus commented out
//  {
//    //defining the pins on the Arduino, delays inbetween for discharging reasons
//
//    analogRead(A5); // current pin
//    delay(10); // delay for discharging reasons
//    int curr_temp = analogRead(A5);
//    int current = 3.3*curr_temp/1024/0.0487;
//    float_msg.data=current + 10000; // offset 10000 to distinguish from other signals
//    chatter.publish(&float_msg);
//    previousMillis4=currentMillis; // update time
//  }

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////  
  
  nh.spinOnce();
  //delay(1); //commented out because we use millis for timing now
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// functions for pressure sensor reading. for better understanding, refer to the document "89BSD Calculation Method"
// which can be found at: http://meas-spec.com/downloads/89BSD_Calculation_Method.pdf

void Calibration()
{
  ReadCalib();
  ReadBits(c0, 1, 15, 14);
  ReadBits(c1, 1, 1, 14);
  ReadBits(c2, 2, 3, 10);
  ReadBits(c3, 3, 9, 10);
  ReadBits(c4, 4, 15, 10);
  ReadBits(c5, 4, 5, 10);
  ReadBits(c6, 5, 11, 10);
  ReadBits(a0, 5, 1, 10);
  ReadBits(a1, 6, 7, 10);
  ReadBits(a2, 7, 13, 10);
}

void ReadBits(int &result, int address_num, int bitbegin, int len)  //address_num and bitbegin specify where the first bit of result lies, len specifies length of the parameter (number of bits)
{
  address_num-=1;

  if (bitRead(data[address_num], bitbegin) == 1) {      //check if data[i] represents a negative number
    result = -1;                                        //invert all bits since value is negative (two's complement)
  }
  
  int counter = 0;
  int counter2 = 0;

  for (int i = bitbegin; (i >= 0) && counter < len; i--)
  {
    bitWrite(result, len - 1 - counter, bitRead(data[address_num], i));
    counter++;
  }
  while (counter < len)
  {
    bitWrite(result, len - 1 - counter, bitRead(data[address_num + 1], 15 - counter2));
    counter++;
    counter2++;
  }
}

void ReadCalib()
{
  byte firstbyte, secondbyte;
  for (int i = 0; i < 7; i++)
  {
    Wire.beginTransmission(BSD_ADDRESS);
    Wire.write(0xA2 + 2*i);
    Wire.endTransmission();
  
    Wire.requestFrom(BSD_ADDRESS, 2);
    if (Wire.available())
    {
      firstbyte = Wire.read();
      secondbyte = Wire.read();
    }
    //data[i] = firstbyte<<8 | secondbyte;  //this method caused problems before, so it is replaced by single bit operations

    int counter = 15; //16 bits to read (0-15)
    for (int k = 7; k >= 0; k--)
    {
      bitWrite(data[i], counter, bitRead(firstbyte, k));
      counter--;
    }
    for (int k = 7; k >= 0; k--)
    {
      bitWrite(data[i], counter, bitRead(secondbyte, k));
      counter--;
    }
  }
}

unsigned long ReadADC(unsigned char address)
{
  byte firstbyte, secondbyte, thirdbyte;
  
  Wire.beginTransmission(BSD_ADDRESS);
  Wire.write(address);
  Wire.endTransmission();
  delay(3);
  Wire.beginTransmission(BSD_ADDRESS);
  Wire.write(0x00);
  Wire.endTransmission();
  
  Wire.requestFrom(BSD_ADDRESS, 3);
  if (Wire.available())
  {
    firstbyte = Wire.read();
    secondbyte = Wire.read();
    thirdbyte = Wire.read();
  }

  unsigned long result = 0;
  int counter = 23; //24 bits to read (0-23)
  for (int i = 7; i >= 0; i--)
  {
    bitWrite(result, counter, bitRead(firstbyte, i));
    counter--;
  }
  for (int i = 7; i >= 0; i--)
  {
    bitWrite(result, counter, bitRead(secondbyte, i));
    counter--;
  }
  for (int i = 7; i >= 0; i--)
  {
    bitWrite(result, counter, bitRead(thirdbyte, i));
    counter--;
  }
  
  return result;
}

void Compensate()
{
  //temp = a0 / 5 + a1 * 2 * d2 / pow(2, 24) + a2 * 2 * pow(d2 / pow(2, 24), 2);
  
  double nom = (d1 + c0 * pow(2, 9) + c3 * pow(2, 15) * d2 / pow(2, 24) + c4 * pow(2, 15) * pow(d2 / pow(2, 24), 2));
  double denom = (c1 * pow(2, 11) + c5 * pow(2, 16) * d2 / pow(2, 24) + c6 * pow(2, 16) * pow(d2 / pow(2, 24), 2));
  double y = nom/denom;
  double p = y * (1 - c2 * pow(2, 9) / pow(2, 24)) + c2 * pow(2, 9) / pow(2, 24) * pow(y, 2);

  p_compensated = (p - 0.1) * 7.5;
}
