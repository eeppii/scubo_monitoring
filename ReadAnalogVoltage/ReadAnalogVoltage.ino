//#define USE_USBCON
//#include <ros.h>
#include <ArduinoHardware.h>
#include <std_msgs/String.h>


void setup() {
  
  Serial.begin(9600); // initialize serial communication at 9600 bits per second
  
}

// the loop routine runs over and over again forever

void loop() {
  
  // read the input on analog pin 0:
  int s1 = analogRead(A0);
  int s2 = analogRead(A1);
  int s3 = analogRead(A2);
  int s4 = analogRead(A3);
  
  // Convert the analog reading (which goes from 0 - 1023) to a voltage (0 - 5V):
  float v1= s1 * (5.0 / 1023.0);
  float v2= s2 * (5.0 / 1023.0);
  float v3= s3 * (5.0 / 1023.0);
  float v4= s4 * (5.0 / 1023.0);
  
  if(v1>3 ||v2>3 ||v3 >3||v4>3) { // print out the value you read
    
    Serial.println("Wasser!");
  
  }

  else {
    
    Serial.println("trocken");

  }

  delay(500);
}
