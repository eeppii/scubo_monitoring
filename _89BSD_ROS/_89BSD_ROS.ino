#if defined(ARDUINO) && ARDUINO >= 100
  #include "Arduino.h"
#else
  #include <WProgram.h>
#endif

#define USE_USBCON  //only when running on Arduino DUE
#include <Wire.h>
#include <math.h>
#include <ros.h>
#include <ArduinoHardware.h>
#include <std_msgs/Float32.h>

#define BSD_ADDRESS 0x77  // I2C address of 89BSD

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Calibration values
int data[7] = {0, 0, 0, 0, 0, 0, 0};
int c0=0;
int c1=0; 
int c2=0; 
int c3=0;
int c4=0;
int c5=0;
int c6=0; 
int a0=0;
int a1=0;
int a2=0;

unsigned long d1;
unsigned long d2;

double temp;
double y;
double p;
double p_compensated;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

ros::NodeHandle nh;
std_msgs::Float32 pressure;
std_msgs::Float32 bsd_temp;
ros::Publisher publish_p("pressure", &pressure); //definition pressure topic
ros::Publisher publish_t("bsd_temp", &bsd_temp);

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void setup() {
  nh.initNode();
  nh.advertise(publish_p);
  nh.advertise(publish_t);
  Wire.begin();
  Wire.beginTransmission(BSD_ADDRESS);
  Wire.write(0x1E);                        //reset command
  Wire.endTransmission();
  delay(100);                      
  
  Calibration();                           //get calibration values
}

void loop() {
  d1 = ReadADC(0x44);
  d2 = ReadADC(0x54);;
  Compensate();
  pressure.data = p_compensated;
  bsd_temp.data = temp;
  publish_p.publish(&pressure);
  publish_t.publish(&bsd_temp);
  nh.spinOnce();
  delay(1000);
}


void Calibration()
{
  ReadCalib();
  ReadBits(c0, 1, 15, 14);
  ReadBits(c1, 1, 1, 14);
  ReadBits(c2, 2, 3, 10);
  ReadBits(c3, 3, 9, 10);
  ReadBits(c4, 4, 15, 10);
  ReadBits(c5, 4, 5, 10);
  ReadBits(c6, 5, 11, 10);
  ReadBits(a0, 5, 1, 10);
  ReadBits(a1, 6, 7, 10);
  ReadBits(a2, 7, 13, 10);
}

void ReadBits(int &result, int address_num, int bitbegin, int len)
{
  address_num-=1;

  if (bitRead(data[address_num], bitbegin) == 1) {      //check if data[i] represents a negative number
    result = -1;                                        //invert all bits since value is negative
//    Serial.print("negativer wert. setze result auf: ");
//    Serial.println(result, BIN);
  }
  
  int counter = 0;
  int counter2 = 0;

  for (int i = bitbegin; (i >= 0) && counter < len; i--)
  {
    bitWrite(result, len - 1 - counter, bitRead(data[address_num], i));
    counter++;
  }
  while (counter < len)
  {
    bitWrite(result, len - 1 - counter, bitRead(data[address_num + 1], 15 - counter2));
    counter++;
    counter2++;
  }
}

void ReadCalib()
{
  byte firstbyte, secondbyte;
  for (int i = 0; i < 7; i++)
  {
    Wire.beginTransmission(BSD_ADDRESS);
    Wire.write(0xA2 + 2*i);
    Wire.endTransmission();
    delay(20);
  
    Wire.requestFrom(BSD_ADDRESS, 2);
    firstbyte = Wire.read();
    secondbyte = Wire.read();
    data[i] = firstbyte<<8 | secondbyte;
  }
}

unsigned long ReadADC(unsigned char address)
{
  byte firstbyte, secondbyte, thirdbyte;
  
  Wire.beginTransmission(BSD_ADDRESS);
  Wire.write(address);
  Wire.endTransmission();
  delay(20);
  Wire.beginTransmission(BSD_ADDRESS);
  Wire.write(0x00);
  Wire.endTransmission();
  delay(20);
  
  Wire.requestFrom(BSD_ADDRESS, 3);
  delay(20);
  
  firstbyte = Wire.read();
  secondbyte = Wire.read();
  thirdbyte = Wire.read();
  delay(10);
  
  return (unsigned long) firstbyte<<16 | secondbyte<<8 | thirdbyte;
}

void Compensate()
{
  temp = a0 / 5 + a1 * 2 * d2 / pow(2, 24) + a2 * 2 * pow(d2 / pow(2, 24), 2);
  
  double nom = (d1 + c0 * pow(2, 9) + c3 * pow(2, 15) * d2 / pow(2, 24) + c4 * pow(2, 15) * pow(d2 / pow(2, 24), 2));
  double denom = (c1 * pow(2, 11) + c5 * pow(2, 16) * d2 / pow(2, 24) + c6 * pow(2, 16) * pow(d2 / pow(2, 24), 2));
  y = nom/denom;
  p = y * (1 - c2 * pow(2, 9) / pow(2, 24)) + c2 * pow(2, 9) / pow(2, 24) * pow(y, 2);

  p_compensated = (p - 0.1) / 0.8 * (6 - 0) + 0;
}
