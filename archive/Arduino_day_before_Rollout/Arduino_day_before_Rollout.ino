//johann, 21. may 2016
//timo, 25. may 2016

#if defined(ARDUINO) && ARDUINO >= 100
  #include "Arduino.h"
#else
  #include <WProgram.h>
#endif


#define USE_USBCON      //needed when running on the UDOO or on Arduino Due, comment for Arduino Uno
#define BSD_ADDRESS 0x77      //I2C address of 89BSD pressure sensor

#include <Servo.h> 
#include <ros.h>
#include <ArduinoHardware.h>
#include <std_msgs/String.h>
#include <std_msgs/UInt16MultiArray.h>
#include <std_msgs/Float32.h>
#include <std_msgs/Float64.h>
#include <Wire.h>
#include <Adafruit_PWMServoDriver.h>
#include <math.h>

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

unsigned int hz_pressure = 100;
unsigned int millis_pressure = 1000 / hz_pressure;

int data[7] = {0, 0, 0, 0, 0, 0, 0};  //pressure sensor calibration values
int c0=0;
int c1=0; 
int c2=0; 
int c3=0;
int c4=0;
int c5=0;
int c6=0; 
int a0=0;
int a1=0;
int a2=0;

unsigned long d1; //digital pressure value (uncompensated)
unsigned long d2; //digital temperature

//double temp;  //actual temperature in celsius
double p_compensated; //temperature-compensated pressure
unsigned long next_pub_time;  // used to get fixed publisher frequency

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//ros::NodeHandle nh; //instantiate node handle, set up the serial port communication
ros::NodeHandle_<ArduinoHardware, 1, 2, 150, 150> nh; // from: http://answers.ros.org/question/28890/using-rosserial-for-a-atmega168arduino-based-motorcontroller/, first two numbers # Publisher/Subscriber , second two numbers are buffersize

Adafruit_PWMServoDriver pwm = Adafruit_PWMServoDriver(); //default adress 0x40 called, adress of break-out board

std_msgs::Float64 float_msg;
std_msgs::Float32 pressure;
//std_msgs::Float32 bsd_temp;

ros::Publisher chatter("sensor_signal", &float_msg);  //definition sensor topic, float64
ros::Publisher publish_p("pressure_signal", &pressure);      //definition pressure topic, float32
//ros::Publisher publish_t("bsd_temp", &bsd_temp);      //temperature of the pressure sensor

unsigned long currentMillis;  //will store time when void loop begins
unsigned long previousMillis1; //will store last publish time, voltage
unsigned long previousMillis2; //will store last publish time, leakage

float bvm_value; // initialization of a voltage variable

float resistor1=402;

float resistor2=95.2;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void pwm_cb( const std_msgs::UInt16MultiArray& cmd_msg) { //callback function of thruster allocation
  
  for(int i=0; i<8; i++) { //runs every thrusters via for-loop
     
     pwm.setPWM(i,0, cmd_msg.data[i]); //PWM signal, state where it goes high, state where it goes low, 0-4095, deadband 351-362
  
  }
  
}

ros::Subscriber<std_msgs::UInt16MultiArray> sub("pwm_signal", pwm_cb); //definition PWM topic and callback function

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void setup() { //this function runs first and only one time on each upload
    
  nh.initNode();
  
  delay(5000); //initialization after approximately 5 seconds
  
  nh.advertise(chatter); //for voltage sensor
  nh.advertise(publish_p); //for pressure sensor
  
  nh.subscribe(sub); //for thruster allocation
    
  pwm.begin();
  
  pwm.setPWMFreq(60); //60 Hz PWM signal
  
  for(int j=0;j<8;j++) {  //initialize every thruster via channel (0-7) with a for-loop 
  
    pwm.setPWM(j,0,351);
    
  }

  Wire.begin();
  Wire.beginTransmission(BSD_ADDRESS);
  Wire.write(0x1E);                        //reset pressure sensor
  Wire.endTransmission();
  delay(10);                      
  
  Calibration();                           //get calibration values

  currentMillis=millis();
  next_pub_time = currentMillis + millis_pressure;
  previousMillis1 = currentMillis - 15000;
  previousMillis2 = currentMillis;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void loop() {
  
  currentMillis=millis();
  
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////  

  if (next_pub_time < currentMillis)
  {
    if (next_pub_time + 10 < currentMillis) 
    {
      next_pub_time = currentMillis + millis_pressure;
    }
    else
    {
      next_pub_time += millis_pressure;
    }    
    
    d1 = ReadADC(0x44);
    d2 = ReadADC(0x54);;
    Compensate();
    //bsd_temp.data = temp;
    pressure.data = p_compensated;
    //publish_t.publish(&bsd_temp);
    publish_p.publish(&pressure);
    //nh.spinOnce();
  }

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  
  if (currentMillis - previousMillis1 > 30000)
  {

    //defining the pins on the Arduino, delays inbetween for discharging reasons

    //voltage
    analogRead(A1);
    delay(10);
    int voltvalue = analogRead(A1);
    bvm_value = 3.3*voltvalue/1024*(resistor1/resistor2 + 1); // change this to 3.3 if running on UDOO or Arduino Due  

    float_msg.data=bvm_value*1000;  //factor 1000 to distinguish from leak signal
  
    chatter.publish(&float_msg);

    // temperature 
    analogRead(A0);
    delay(10);
    int vo = analogRead(A0);
    float temp_value = (3.3*vo/1024 - 0.25)*1000/28 - 190*1.5/1000;  // transfer function and heat sink correction (190*1.5/1000)

    float_msg.data=temp_value*1000; //factor 1000 to distinguish from leak signal
  
    chatter.publish(&float_msg);
   
    previousMillis1=currentMillis; // update time

    //nh.spinOnce();
  }
      
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////  
      
  if (currentMillis - previousMillis2 > 30000)
  {  
         
    //defining the pins on the Arduino, delays inbetween for discharging reasons
    
    analogRead(A2); //leak pin
    delay(10); // delay for discharging reasons, previously 50
    int leakvalue=analogRead(A2); // defining the pins on the Arduino
    float leakage=leakvalue*(3.3/1023); // change this to 3.3 if running on UDOO or Arduino Due

    float_msg.data=leakage;
    
    chatter.publish(&float_msg);

    analogRead(A3); //leak pin
    delay(10); // delay for discharging reasons, previously 50
    leakvalue=analogRead(A3); // defining the pins on the Arduino

    leakage=leakvalue*(3.3/1023); // change this to 3.3 if running on UDOO or Arduino Due

    float_msg.data=leakage;
    
    chatter.publish(&float_msg);

    analogRead(A4); //leak pin
    delay(10); // delay for discharging reasons, previously 50
    leakvalue=analogRead(A4); // defining the pins on the Arduino

    leakage=leakvalue*(3.3/1023); // change this to 3.3 if running on UDOO or Arduino Due

    float_msg.data=leakage;
    
    chatter.publish(&float_msg);
    
    previousMillis2=currentMillis; // update time

    //nh.spinOnce();        
  }
         
   
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////  
  
  nh.spinOnce();
  //delay(1); //commented because we use millis for timing now
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// functions for pressure sensor reading

void Calibration()
{
  ReadCalib();
  ReadBits(c0, 1, 15, 14);
  ReadBits(c1, 1, 1, 14);
  ReadBits(c2, 2, 3, 10);
  ReadBits(c3, 3, 9, 10);
  ReadBits(c4, 4, 15, 10);
  ReadBits(c5, 4, 5, 10);
  ReadBits(c6, 5, 11, 10);
  ReadBits(a0, 5, 1, 10);
  ReadBits(a1, 6, 7, 10);
  ReadBits(a2, 7, 13, 10);
}

void ReadBits(int &result, int address_num, int bitbegin, int len)  //address_num and bitbegin specify where the first bit of result lies, len specifies length of the parameter (number of bits)
{
  address_num-=1;

  if (bitRead(data[address_num], bitbegin) == 1) {      //check if data[i] represents a negative number
    result = -1;                                        //invert all bits since value is negative (two's complement)
  }
  
  int counter = 0;
  int counter2 = 0;

  for (int i = bitbegin; (i >= 0) && counter < len; i--)
  {
    bitWrite(result, len - 1 - counter, bitRead(data[address_num], i));
    counter++;
  }
  while (counter < len)
  {
    bitWrite(result, len - 1 - counter, bitRead(data[address_num + 1], 15 - counter2));
    counter++;
    counter2++;
  }
}

void ReadCalib()
{
  byte firstbyte, secondbyte;
  for (int i = 0; i < 7; i++)
  {
    Wire.beginTransmission(BSD_ADDRESS);
    Wire.write(0xA2 + 2*i);
    Wire.endTransmission();
  
    Wire.requestFrom(BSD_ADDRESS, 2);
    if (Wire.available())
    {
      firstbyte = Wire.read();
      secondbyte = Wire.read();
    }
    //data[i] = firstbyte<<8 | secondbyte;  //this method caused problems before, so it is replaced by single bit operations

    int counter = 15; //16 bits to read (0-15)
    for (int k = 7; k >= 0; k--)
    {
      bitWrite(data[i], counter, bitRead(firstbyte, k));
      counter--;
    }
    for (int k = 7; k >= 0; k--)
    {
      bitWrite(data[i], counter, bitRead(secondbyte, k));
      counter--;
    }
  }
}

unsigned long ReadADC(unsigned char address)
{
  byte firstbyte, secondbyte, thirdbyte;
  
  Wire.beginTransmission(BSD_ADDRESS);
  Wire.write(address);
  Wire.endTransmission();
  delay(3);
  Wire.beginTransmission(BSD_ADDRESS);
  Wire.write(0x00);
  Wire.endTransmission();
  
  Wire.requestFrom(BSD_ADDRESS, 3);
  if (Wire.available())
  {
    firstbyte = Wire.read();
    secondbyte = Wire.read();
    thirdbyte = Wire.read();
  }

  unsigned long result = 0;
  int counter = 23; //24 bits to read (0-23)
  for (int i = 7; i >= 0; i--)
  {
    bitWrite(result, counter, bitRead(firstbyte, i));
    counter--;
  }
  for (int i = 7; i >= 0; i--)
  {
    bitWrite(result, counter, bitRead(secondbyte, i));
    counter--;
  }
  for (int i = 7; i >= 0; i--)
  {
    bitWrite(result, counter, bitRead(thirdbyte, i));
    counter--;
  }
  
  return result;
}

void Compensate()
{
  //temp = a0 / 5 + a1 * 2 * d2 / pow(2, 24) + a2 * 2 * pow(d2 / pow(2, 24), 2);
  
  double nom = (d1 + c0 * pow(2, 9) + c3 * pow(2, 15) * d2 / pow(2, 24) + c4 * pow(2, 15) * pow(d2 / pow(2, 24), 2));
  double denom = (c1 * pow(2, 11) + c5 * pow(2, 16) * d2 / pow(2, 24) + c6 * pow(2, 16) * pow(d2 / pow(2, 24), 2));
  double y = nom/denom;
  double p = y * (1 - c2 * pow(2, 9) / pow(2, 24)) + c2 * pow(2, 9) / pow(2, 24) * pow(y, 2);

  p_compensated = (p - 0.1) * 7.5;
}
